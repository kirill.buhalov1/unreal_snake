// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"

#include "SnakeBase.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
    PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90.0f, 0.0f, 0.0f));

	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::MoveVertical);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::MoveHorizontal);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::MoveVertical(float x)
{
	if (SnakeActor->bIsDirectionUpdating)
	{
		return;
	}
	
	if (IsValid(SnakeActor))
	{
		if (x > 0 && SnakeActor->Direction != EDirection::Down)
		{
			UpdateDirection(EDirection::Up);
		}
		else if (x < 0 && SnakeActor->Direction != EDirection::Up)
		{
			UpdateDirection(EDirection::Down);
		}
	}
}

void APlayerPawnBase::MoveHorizontal(float y)
{
	if (SnakeActor->bIsDirectionUpdating)
	{
		return;
	}
	
	if (IsValid(SnakeActor))
	{
		if (y > 0 && SnakeActor->Direction != EDirection::Left)
		{
			UpdateDirection(EDirection::Right);
		}
		else if (y < 0 && SnakeActor->Direction != EDirection::Right)
		{
			UpdateDirection(EDirection::Left);
		}
	}
}

void APlayerPawnBase::UpdateDirection(EDirection NewDirection)
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->Direction = NewDirection;
		SnakeActor->bIsDirectionUpdating = true;
	}
}

