// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SnakeElementSize = 60.0f;
	MoveSpeed = 0.5f;
	Direction = EDirection::Up;
	bIsDirectionUpdating = false;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveSpeed);
	AddSnakeElement(3);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	Move();
}

void ASnakeBase::AddSnakeElement(int amount)
{
	if(amount == 0)
	{
		return;
	}
	
	if (amount < 0)
	{
		if (SnakeElements.Num() > 2)
		{
			SnakeElements.Last()->Destroy();
			SnakeElements.Pop();
		}
		else
		{
			Die();
		}

		return;
	}
	
	for (int i = 0; i < amount; i++)
	{
		FVector SpawnLocation = FVector(0.0f, 0.0f, 0.0f);
		if (SnakeElements.Num() == 0)
		{
			SpawnLocation = FVector(0.0f, 0.0f, 0.0f);
		}
		else if (SnakeElements.Num() == 1)
		{
			SpawnLocation = FVector(- SnakeElementSize, 0.0f, 0.0f);
		}
		else
		{
			auto delta = SnakeElements.Last()->GetActorLocation() - SnakeElements.Last(1)->GetActorLocation();
			SpawnLocation = SnakeElements.Last()->GetActorLocation() + delta;
		}

		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(SpawnLocation));
		NewElement->SnakeOwner = this;
		SnakeElements.Add(NewElement);
		if(SnakeElements.Num() == 1)
		{
			NewElement->SetSnakeHead();
			NewElement->SetSnakeHead_Implementation();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementStep = SnakeElements[0]->GetActorLocation();

	switch (Direction)
	{
	case EDirection::Up:
		MovementStep.X += SnakeElementSize;
		break;
	case EDirection::Down:
		MovementStep.X -= SnakeElementSize;
		break;
	case EDirection::Left:
		MovementStep.Y -= SnakeElementSize;
		break;
	case EDirection::Right:
		MovementStep.Y += SnakeElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision(false);

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		FVector NextLocation = SnakeElements[i - 1]->GetActorLocation();
		SnakeElements[i]->SetActorLocation(NextLocation);
	}

	SnakeElements[0]->SetActorLocation(MovementStep);
	SnakeElements[0]->ToggleCollision(true);

	bIsDirectionUpdating = false;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if(IsValid(OverlappedElement))
	{
		bool bIsHead = OverlappedElement == SnakeElements[0];
		
		IInteractable* Interactable = Cast<IInteractable>(OtherActor);
		if(Interactable)
		{
			Interactable->Interact(this, bIsHead);
		}
	}
}

void ASnakeBase::Die()
{
	for (auto Element : SnakeElements)
	{
		Element->Destroy();
	}
	SnakeElements.Empty();

	AddSnakeElement(3);
} 

